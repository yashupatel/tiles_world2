<!-- footer -->
<div class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="footer-grids">
                <div class="col-md-3 col-sm-6 footer-logo">
                    <div class="agileits-logo">
                        <h2><a href="index.php">Tiles World</a></h2>
                    </div>
                    <p>A-26, Sara Nagar,<br>Opp. Yash Complex,<br>30 mt. Road,<br>Gotri,<br>Vadodara - 390 021.</p>
                </div>
                <div class="col-md-3 col-sm-6 footer-grid">
                    <h3>Navigation</h3>
                    <ul>
                        <li><a href="index.php">Home</a> </li>
                        <li><a href="about.php">About</a></li>
                        <li><a href="contact.php">Contact US</a> </li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 footer-grid">
                    <h3>Our Product</h3>
                    <ul>
                        <li><a href="tilesware.php">Tilesware</a></li>
                        <li><a href="sanitryware.php">Sanitryware</a></li>
                        <li><a href="bathware.php">Bathware</a></li>
                        <li><a href="kitchenware.php">Kitchenware</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 footer-grid insta-mobile">
                    <h3>Instagram Post</h3>
                    <ul>
                        <li><a href="blog&post.php">Blog And Post</a> </li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <p>© 2020 Tiles World. All rights reserved | Design by <b><a
                    href="http://4foxwebsolution.com/">4FOXWEBSOLUTIONS</a></b></p>
        </div>
    </div>
    <div id="WAButton"></div>

</div>
<!-- //footer -->

<script type="text/javascript">
$(function(){
    $('#WAButton').floatingWhatsApp({
        phone: '+91 9327614922', //WhatsApp Business phone number International format-
        //Get it with Toky at https://toky.co/en/features/whatsapp.
        headerTitle: 'Chat with us on WhatsApp!', //Popup Title
        popupMessage: 'Hello!!! How can we help you?', //Popup Message
        showPopup: true, //Enables popup display
        buttonImage: '<img src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/whatsapp.svg" />', //Button Image
        //headerColor: 'crimson', //Custom header color
        //backgroundColor: 'crimson', //Custom background button color
        position: "right",
        size: "50px"
    });
});
</script>