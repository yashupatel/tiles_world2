<?php
 include('include/header.php'); 
?>
</div>
</div>
<!-- //banner -->
<!-- gallery -->
<div class="gallery team">
    <div class="container">
        <h3 class="agileits-title w3title2">Tilesware</h3>
        <div class="agile_gallery_grids">
            <div class="col-sm-4 col-xs-4 agile_gallery_grid">
                <div class="w3ls_gallery_grid">
                    <a href="images/tilesware_1_0.jpg" class="lsb-preview wthree_p_grid" data-lsb-group="header">
                        <img src="images/tilesware_1_0.jpg" alt=" " class="img-responsive" />
                        <div class="agileinfo_content_wrap">
                            <div class="agileits_content">
                                <h3>Tilesware</h3>
                                <p>600 X 600</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="w3ls_gallery_grid">
                    <a href="images/tilesware_5_0.jpg" class="lsb-preview wthree_p_grid" data-lsb-group="header">
                        <img src="images/tilesware_5_0.jpg" alt=" " class="img-responsive" />
                        <div class="agileinfo_content_wrap">
                            <div class="agileits_content">
                                <h3>Tilesware</h3>
                                <p>800 X 800</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="w3ls_gallery_grid">
                    <a href="images/tilesware_3_0.jpg" class="lsb-preview wthree_p_grid" data-lsb-group="header">
                        <img src="images/tilesware_3_0.jpg" alt=" " class="img-responsive" />
                        <div class="agileinfo_content_wrap">
                            <div class="agileits_content">
                                <h3>Tilesware</h3>
                                <p>600 X 600</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-4 col-xs-4 agile_gallery_grid">
                <div class="w3ls_gallery_grid">
                    <a href="images/tilesware_4_0.jpg" class="lsb-preview wthree_p_grid" data-lsb-group="header">
                        <img src="images/tilesware_4_0.jpg" alt=" " class="img-responsive" />
                        <div class="agileinfo_content_wrap">
                            <div class="agileits_content">
                                <h3>Tilesware</h3>
                                <p>600 X 600</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="w3ls_gallery_grid">
                    <a href="images/tilesware_2_0.jpg" class="lsb-preview wthree_p_grid" data-lsb-group="header">
                        <img src="images/tilesware_2_0.jpg" alt=" " class="img-responsive" />
                        <div class="agileinfo_content_wrap">
                            <div class="agileits_content">
                                <h3>Tilesware</h3>
                                <p>800 X 800</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="w3ls_gallery_grid">
                    <a href="images/tilesware_6_0.jpg" class="lsb-preview wthree_p_grid" data-lsb-group="header">
                        <img src="images/tilesware_6_0.jpg" alt=" " class="img-responsive" />
                        <div class="agileinfo_content_wrap">
                            <div class="agileits_content">
                                <h3>Tilesware</h3>
                                <p>600 X 600</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-4 col-xs-4 agile_gallery_grid">
                <div class="w3ls_gallery_grid">
                    <a href="images/tilesware_7_0.jpg" class="lsb-preview wthree_p_grid" data-lsb-group="header">
                        <img src="images/tilesware_7_0.jpg" alt=" " class="img-responsive" />
                        <div class="agileinfo_content_wrap">
                            <div class="agileits_content">
                                <h3>Tilesware</h3>
                                <p>600 X 600</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="w3ls_gallery_grid">
                    <a href="images/tilesware_8_0.jpg" class="lsb-preview wthree_p_grid" data-lsb-group="header">
                        <img src="images/tilesware_8_0.jpg" alt=" " class="img-responsive" />
                        <div class="agileinfo_content_wrap">
                            <div class="agileits_content">
                                <h3>Tilesware</h3>
                                <p>800 X 800</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="w3ls_gallery_grid">
                    <a href="images/tilesware_9_0.jpg" class="lsb-preview wthree_p_grid" data-lsb-group="header">
                        <img src="images/tilesware_9_0.jpg" alt=" " class="img-responsive" />
                        <div class="agileinfo_content_wrap">
                            <div class="agileits_content">
                                <h3>Tilesware</h3>
                                <p>600 X 600</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //gallery -->
<!-- footer -->
<?php include('include/footer.php'); ?>
	<!-- js --> 
	<!-- js --> 
	<!-- <script src="js/jquery-2.2.3.min.js"></script>   -->
	<!-- //js -->  
	<!-- ResponsiveTabs js -->
	<!-- <script src="js/easyResponsiveTabs.js" type="text/javascript"></script> -->
	<script type="text/javascript">
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion           
				width: 'auto', //auto or any width like 600px
				fit: true   // 100% fit in a container
			});
		});
	</script>
	<!-- //ResponsiveTabs js -->  
	<!-- jarallax -->  
	<!-- <script src="js/SmoothScroll.min.js"></script> 
	<script src="js/jarallax.js"></script>  -->
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed: 0.5,
			imgWidth: 1366,
			imgHeight: 768
		})
	</script>  
	<!-- //jarallax -->  
	<!-- gallery-lightbox -->  
	<script>
	$(window).load(function() {
		  $.fn.lightspeedBox();
		});
	</script> 
	<!-- //gallery-lightbox -->  
	<!-- start-smooth-scrolling -->
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
			
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
	<!-- //end-smooth-scrolling -->	
	<!-- smooth-scrolling-of-move-up -->
	<!-- //smooth-scrolling-of-move-up --> 
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
</body>
</html>