<?php
 include('include/header.php'); 
  ?> 	
			<!-- //header -->  
		</div>	
	</div>	
	<!-- //banner --> 
	<!-- gallery -->
	<div class="gallery team">
		<div class="container"> 
			<h3 class="agileits-title w3title2">Kitchen Ware</h3>
			<div class="agile_gallery_grids">	
				<div class="col-sm-4 col-xs-4 agile_gallery_grid"> 
					<div class="w3ls_gallery_grid">
						<a href="images/kitchenware_2_0.jpeg" class="lsb-preview wthree_p_grid" data-lsb-group="header">  
							<img src="images/kitchenware_2_0.jpeg" alt=" " class="img-responsive" />
							<div class="agileinfo_content_wrap">
								<div class="agileits_content">		
									<h3>Kitchenware</h3>
									<p>Built Your Kitchen</p>
								</div>
							</div> 
						</a>
					</div>
				</div>
				<div class="col-sm-4 col-xs-4 agile_gallery_grid"> 
					<div class="w3ls_gallery_grid">
						<a href="images/kitchenware_1_0.jpg" class="lsb-preview wthree_p_grid" data-lsb-group="header">
							<img src="images/kitchenware_1_0.jpg" alt=" " class="img-responsive" />
							<div class="agileinfo_content_wrap">
								<div class="agileits_content">		
									<h3>Kitchenware</h3>
									<p>Built Your Kitchen</p>
								</div>
							</div> 
						</a>
					</div>
				</div>
				<div class="col-sm-4 col-xs-4 agile_gallery_grid">
					<div class="w3ls_gallery_grid">
						<a href="images/kitchenware_3_0.jpg" class="lsb-preview wthree_p_grid" data-lsb-group="header">
							<img src="images/kitchenware_3_0.jpg" alt=" " class="img-responsive" />
							<div class="agileinfo_content_wrap">
								<div class="agileits_content">		
									<h3>Kitchenware</h3>
									<p>Built Your Kitchen</p>
								</div>
							</div> 
						</a>
					</div>
				</div> 
				<div class="clearfix"> </div>
			</div> 
		</div>
	</div>
	<!-- //gallery -->  
	<!-- news letter -->
	<!-- //news letter -->
	<!-- footer -->
	<?php include('include/footer.php'); ?>

	<!-- //footer -->
	<!-- modal-about -->
	<!-- //modal-about -->
	<!-- modal sign in  -->
	<!-- //modal sign in -->  
	<!-- js --> 
	<!-- <script src="js/jquery-2.2.3.min.js"></script>   -->
	<!-- //js -->  
	<!-- ResponsiveTabs js -->
	<!-- <script src="js/easyResponsiveTabs.js" type="text/javascript"></script> -->
	<script type="text/javascript">
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion           
				width: 'auto', //auto or any width like 600px
				fit: true   // 100% fit in a container
			});
		});
	</script>
	<!-- //ResponsiveTabs js -->  
	<!-- jarallax -->  
	<!-- <script src="js/SmoothScroll.min.js"></script> 
	<script src="js/jarallax.js"></script>  -->
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed: 0.5,
			imgWidth: 1366,
			imgHeight: 768
		})
	</script>  
	<!-- //jarallax -->  
	<!-- gallery-lightbox -->  
	<!-- <script src="js/lsb.min.js"></script> -->
	<script>
	$(window).load(function() {
		  $.fn.lightspeedBox();
		});
	</script> 
	<!-- //gallery-lightbox -->  
	<!-- start-smooth-scrolling -->
	<!-- <script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>	 -->
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
			
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
	<!-- //end-smooth-scrolling -->	
	<!-- smooth-scrolling-of-move-up -->
	<!-- <script type="text/javascript">
		$(document).ready(function() {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script> -->
	<!-- //smooth-scrolling-of-move-up --> 
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="js/bootstrap.js"></script> -->
</body>
</html>