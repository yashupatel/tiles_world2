<?php
 include('include/header.php'); 
?>
</div>
</div>
<!-- //banner -->
<!-- welcome -->
<br>
<center>
    <h1 class="text-dark mb-2">About Us</h1>
</center>

<!-- <section class="bottom-banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 about-img-pages">
            </div>
            <div class="col-lg-4 about-info">

                <div class="service-in text-left">
                    <div>
                        <div class="card-body" style="padding:0px">
                            <img src="images/about3.jpg" alt="">
                        </div>
                    </div>
                </div><br><br><br>
                <div class="service-in text-left">
                    <div class="">
                        <div class="card-body" style="padding:0px">
                            <img src="images/about4.jpg" alt="">
                        </div>
                    </div>
                </div> <br><br><br>
                <div class="service-in text-left">
                    <div class="">
                        <div class="card-body" style="padding:0px">
                            <img src="images/about6.jpg" alt="">
                        </div>
                    </div>
                </div><br><br><br><br><br>
            </div>
        </div>
    </div>
</section> -->


<div class="welcome">
    <div class="container">
        <div class="col-md-6 w3ls_welcome_right">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="agileits_w3layouts_welcome_grid">
                            <img src="images/about3.jpg" alt=" " class="img-responsive" />
                        </div>
                    </li>
                    <li>
                        <div class="agileits_w3layouts_welcome_grid">
                            <img src="images/about4.jpg" alt=" " class="img-responsive" />
                        </div>
                    </li>
                    <li>
                        <div class="agileits_w3layouts_welcome_grid">
                            <img src="images/about6.jpg" alt=" " class="img-responsive" />
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-6 w3ls_welcome_left">
            <div class="w3ls_welcome_right1">
                <h3 class="agileits-title text-center">Aris</h3>
                <h6>
                    <p><b class="text-color:black;"> Visionary and innovative</b>, ARIS products are designed to last a
                        life time - investment pieces,
                        conceived to deliver the perfect balance of quality, technology and design. High-quality
                        materials
                        and the latest manufacturing process are combined with industry-leading technologies to ensure
                        years
                        of reliable performance.</p>
                </h6>

                <h6>
                    <p><b class="text-color:black;"> ARIS faucets deliver</b>,incredible performance - inside and out -
                        guaranteed. Combining premium materials with design innovations and rigorous testing, we ensure
                        that our faucets are built to last. Our entire faucets team - from engineering to manufacturing
                        - works with the common goal of producing quality faucets you can depend on for years to come.
                    </p>
                </h6>

            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!-- //welcome -->
<!-- Stats -->

<div class="services jarallax">
    <div class="container">
        <h3 class="agileits-title w3title1">Our Services</h3>
        <div class="services-w3ls-row">
            <div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <span class="glyphicon glyphicon-home effect-1" aria-hidden="true"></span>
                <h5>OUR VISION</h5>
                <p>To Gain International Recognization For Our Products Through Best Quality.</p>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <span class="glyphicon glyphicon-list-alt effect-1" aria-hidden="true"></span>
                <h5>OUR MISSION</h5>
                <p>To Become Most Innovative And Valueable An Brand Ammount Ceramic World.</p>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <span class="glyphicon glyphicon-tree-deciduous effect-1" aria-hidden="true"></span>
                <h5>QUALITY ASSURANCE</h5>
                <p>Company Is Commited To Provide Best Quality Products Being A Quality Assured Firm.</p>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 services-grid">
                <span class="glyphicon glyphicon-globe effect-1" aria-hidden="true"></span>
                <h5>SERVICE</h5>
                <p>We Are Here For Best Services And Client Satisfaction.</p>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
</div>
<!-- <div class="stats services jarallax">
    <div class="container">
        <div class="stats-info agileits-w3layouts">
            <div class="col-sm-3 col-xs-6 stats-grid">
                <div class="stats-img">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
                <h5>Happy Clients</h5>
                <div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='500'
                    data-delay='.5' data-increment="100">500</div>
            </div>
            <div class="col-sm-3 col-xs-6 stats-grid">
                <div class="stats-img w3-agileits">
                    <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                </div>
                <h5>Our Product</h5>
                <div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='50' data-delay='8'
                    data-increment="1">50</div>
            </div>
            <div class="col-sm-3 col-xs-6 stats-grid">
                <div class="stats-img w3-agileits">
                    <i class="fa fa-briefcase" aria-hidden="true"></i>
                </div>
                <h5>Services</h5>
                <div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='99'
                    data-delay='.5' data-increment="100">99%</div>
            </div>
            <div class="col-sm-3 col-xs-6 stats-grid">
                <div class="stats-img w3-agileits">
                    <i class="fa fa-trophy" aria-hidden="true"></i>
                </div>
                <h5>Trust</h5>
                <div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='100' data-delay='8'
                    data-increment="1">100</div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div> -->
<!-- //Stats -->
<!-- team -->
<div class="welcome">
    <div class="container">

        <div class="col-md-6 w3ls_welcome_left">
            <div class="w3ls_welcome_right1">
                <center><img src="images/iball_logo.jpg" alt=""></center>

                <!-- <h3 class="agileits-title text-center">Bell</h3> -->
                <h6>
                    <p><b class="text-color:black;">The first of its kind Indian Brand</b> to be accredited with SIRIM
                        Standard certification</p>
                </h6>

                <h6>
                    <p><b class="text-color:black;"> Bell is certified</b> with the prestigious ISO Certification to
                        enable Quality Management Systems.</p>
                </h6>
                <h6>
                    <p><b class="text-color:black;"> Our product</b> are certified with the latest CE Mark (Conformité
                        Européene).</p>
                </h6>

            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="col-md-6 w3ls_welcome_right">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="agileits_w3layouts_welcome_grid">
                            <img src="images/iball_1.jpg" alt=" " class="img-responsive" />
                        </div>
                    </li>
                    <!-- <li>
                        <div class="agileits_w3layouts_welcome_grid">
                            <img src="images/iball_2.jpg" alt=" " class="img-responsive" />
                        </div>
                    </li> -->
                    <li>
                        <div class="agileits_w3layouts_welcome_grid">
                            <img src="images/iball_3.jpg" alt=" " class="img-responsive" />
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>

<!-- <section class="bottom-banner">
    <div class="container-fluid"    >
        <div class="row">
            <div class="col-lg-6 about-img-pagesss">
            </div>
            <div class="col-lg-6 about-img-pagess">
            </div>
        </div>
    </div>
</section> -->




<!-- <div class="team agileits">
    <div class="team-agileinfo">
        <div class="container">
            <h3 class="agileits-title w3title2">Our Team</h3>
            <div class="team-row agileits-w3layouts">
                <div class="col-sm-3 col-xs-6 team-grids">
                    <div class="team-agileimg">
                        <img class="img-responsive" src="images/t1.jpg" alt="">
                        <div class="captn">
                            <div class="captn-top">
                                <h4>Edwards Doe</h4>
                                <p>Aenean pulvinar ac enimet posuere tincidunt velit Utin tincidunt</p>
                            </div>
                            <div class="social-w3lsicon">
                                <ul class="agileits_social_list">
                                    <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook"
                                                aria-hidden="true"></i></a></li>
                                    <li><a href="#" class="agile_twitter"><i class="fa fa-twitter"
                                                aria-hidden="true"></i></a></li>
                                    <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble"
                                                aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6 team-grids">
                    <div class="team-agileimg">
                        <img class="img-responsive" src="images/t2.jpg" alt="">
                        <div class="captn">
                            <div class="captn-top">
                                <h4>Mark Sophia</h4>
                                <p>Aenean pulvinar ac enimet posuere tincidunt velit Utin tincidunt</p>
                            </div>
                            <div class="social-w3lsicon">
                                <ul class="agileits_social_list">
                                    <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook"
                                                aria-hidden="true"></i></a></li>
                                    <li><a href="#" class="agile_twitter"><i class="fa fa-twitter"
                                                aria-hidden="true"></i></a></li>
                                    <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble"
                                                aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6 team-grids">
                    <div class="team-agileimg">
                        <img class="img-responsive" src="images/t3.jpg" alt="">
                        <div class="captn">
                            <div class="captn-top">
                                <h4>Michael amet</h4>
                                <p>Aenean pulvinar ac enimet posuere tincidunt velit Utin tincidunt</p>
                            </div>
                            <div class="social-w3lsicon">
                                <ul class="agileits_social_list">
                                    <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook"
                                                aria-hidden="true"></i></a></li>
                                    <li><a href="#" class="agile_twitter"><i class="fa fa-twitter"
                                                aria-hidden="true"></i></a></li>
                                    <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble"
                                                aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6 team-grids">
                    <div class="team-agileimg">
                        <img class="img-responsive" src="images/t4.jpg" alt="">
                        <div class="captn">
                            <div class="captn-top">
                                <h4>Daniel Nyari</h4>
                                <p>Aenean pulvinar ac enimet posuere tincidunt velit Utin tincidunt</p>
                            </div>
                            <div class="social-w3lsicon">
                                <ul class="agileits_social_list">
                                    <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook"
                                                aria-hidden="true"></i></a></li>
                                    <li><a href="#" class="agile_twitter"><i class="fa fa-twitter"
                                                aria-hidden="true"></i></a></li>
                                    <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble"
                                                aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div> -->
<!-- //team -->
<!-- footer -->
<?php include('include/footer.php'); ?>
<!-- //footer -->
<script type="text/javascript">
/* init Jarallax */
$('.jarallax').jarallax({
    speed: 0.5,
    imgWidth: 1366,
    imgHeight: 768
})
</script>
<!-- //jarallax -->
<!-- flexSlider -->
<script defer src="js/jquery.flexslider.js"></script>
<script type="text/javascript">
$(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider) {
            $('body').removeClass('loading');
        }
    });
});
</script>
<!-- //flexSlider -->
<script type="text/javascript">
jQuery(document).ready(function($) {
    $(".scroll").click(function(event) {
        event.preventDefault();

        $('html,body').animate({
            scrollTop: $(this.hash).offset().top
        }, 1000);
    });
});
</script>
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->
<script type="text/javascript">
$(document).ready(function() {
    /*
    var defaults = {
    	containerID: 'toTop', // fading element id
    	containerHoverID: 'toTopHover', // fading element hover id
    	scrollSpeed: 1200,
    	easingType: 'linear' 
    };
    */

    // $().UItoTop({
    //     easingType: 'easeOutQuart'
    // });

});
</script>
<!-- //smooth-scrolling-of-move-up -->
<!-- ResponsiveTabs js -->
<script type="text/javascript">
$(document).ready(function() {
    $('#horizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion           
        width: 'auto', //auto or any width like 600px
        fit: true // 100% fit in a container
    });
});
</script>
<!-- //ResponsiveTabs js -->
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<!-- <script type="text/javascript">
$(function(){
    $('#WAButton').floatingWhatsApp({
        phone: '+91 9327614922', //WhatsApp Business phone number International format-
        //Get it with Toky at https://toky.co/en/features/whatsapp.
        headerTitle: 'Chat with us on WhatsApp!', //Popup Title
        popupMessage: 'Hello!!! How can we help you?', //Popup Message
        showPopup: true, //Enables popup display
        buttonImage: '<img src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/whatsapp.svg" />', //Button Image
        //headerColor: 'crimson', //Custom header color
        //backgroundColor: 'crimson', //Custom background button color
        position: "right",
        size: "50px"
    });
}); -->
</script>
</body>

</html>