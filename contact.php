<?php
 include('include/header.php'); 
  ?>  
		</div>	
	</div>	
	<!-- //banner --> 
	<!-- contact -->
	<div class="contact">
		<div class="container">   
			<h3 class="agileits-title w3title2">Contact Us</h3>
			<div class="map-pos">
				<div class="col-md-4 address-row">
					<div class="col-xs-2 address-left">
						<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
					</div>
					<div class="col-xs-10 address-right">
						<h5>Visit Us</h5>
						<p>Vadodara,Gujarat</p>
						<!-- <p></p> -->
					</div>
					
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-4 address-row w3-agileits">
					<div class="col-xs-2 address-left">
						<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
					</div>
					<div class="col-xs-10 address-right">
						<h5>Mail Us</h5>
						<p><a href="mailto:info@example.com">tilesworldvadodara@gmail.com</a></p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-4 address-row">
					<div class="col-xs-2 address-left">
						<span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
					</div>
					<div class="col-xs-10 address-right">
						<h5>Call Us</h5>
						<a  href="tel:+917622014922">
							<p>+91 7622014922</p>
						</a>
					</div>
					<div class="clearfix"> </div>
				</div>   
			</div> 
			<form action="#" method="post">
				<div class="col-sm-6 contact-left">
					<input type="text" name="Name" placeholder="Your Name" required="">
					<input type="email" name="Email" placeholder="Email" required="">
					<input type="text" name="Mobile Number" placeholder="Mobile Number" required="">
				</div>
				<div class="col-sm-6 contact-right"> 
					<textarea name="Message" placeholder="Message" required=""></textarea>
					<input type="submit" value="Submit" >
				</div>
				<div class="clearfix"></div>
			</form>
		</div>
	</div>  
	<!-- //contact --> 
	<div class="map">
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14763.94043812902!2d73.13752499999998!3d22.316402900000014!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1d85ce0ffbe35872!2sTiles%20World!5e0!3m2!1sen!2sin!4v1596273374050!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>	</div> 
	<!-- news letter -->
	<!-- //news letter -->
	<!-- footer -->
	<?php include('include/footer.php'); ?>

	<!-- js --> 	<!-- //js -->  
	<!-- ResponsiveTabs js -->
	<!-- <script src="js/easyResponsiveTabs.js" type="text/javascript"></script> -->
	<script type="text/javascript">
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion           
				width: 'auto', //auto or any width like 600px
				fit: true   // 100% fit in a container
			});
		});
	</script>
	<!-- //ResponsiveTabs js -->  
	<!-- jarallax -->  
	<!-- <script src="js/SmoothScroll.min.js"></script> 
	<script src="js/jarallax.js"></script>  -->
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed: 0.5,
			imgWidth: 1366,
			imgHeight: 768
		})
	</script>  
	<!-- //jarallax -->  
	<!-- start-smooth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>	
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
			
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
	<!-- //end-smooth-scrolling -->	
	<!-- smooth-scrolling-of-move-up -->
	<!-- <script type="text/javascript">
		$(document).ready(function() {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script> -->
	<!-- //smooth-scrolling-of-move-up --> 
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
</body>
</html>