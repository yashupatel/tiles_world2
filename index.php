<?php
 include('include/header.php'); 
?>
<!-- banner-text -->
<div class="banner-text agileinfo">
    <div class="container">
        <div class="agile_banner_info">
            <div class="agile_banner_info1">
                <h6>We are here for best services and client satisfaction. </h6>
                <div id="typed-strings" class="agileits_w3layouts_strings">
                    <p>Welcome to<i> Tiles World</i></p>
                    <p>Built Your Home <i>With US</i></p>
                    <!-- <p>Curabi temlaci <i>Pharetra</i></p> -->
                </div>
                <span id="typed" style="white-space:pre;"></span>
            </div>
        </div>
        <div class="agile_social_icons_banner">
            <ul class="agileits_social_list">
                <li><a href="https://www.facebook.com/tilesworldvadodara" class="w3_agile_facebook" target="_blank"><i
                            class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <!-- <li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li> -->
                <li><a href="https://www.instagram.com/tiles__world_/" class="w3_agile_dribble" target="_blank"><i
                            class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <!-- <li><a href="#" class="w3_agile_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li> -->
            </ul>
        </div>
    </div>
</div>
<!-- //banner-text -->
</div>
</div>
<!-- //banner -->
<!-- banner-bottom -->
<!-- <div class="banner-bottom">
		<div class="col-md-4 bnr-agileitsgrids">
			<div class="agileinfo_banner_bottom_pos">
				<div class="w3_agileits_banner_bottom_pos_grid">
					<div class="col-xs-4 wthree_banner_bottom_grid_left">
						<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
							<i class="fa fa-clone" aria-hidden="true"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_banner_bottom_grid_right">	
						<h4>Free Consultation</h4>
						<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<div class="col-md-4 bnr-agileitsgrids w3grid1">
			<div class="agileinfo_banner_bottom_pos">
				<div class="w3_agileits_banner_bottom_pos_grid">
					<div class="col-xs-4 wthree_banner_bottom_grid_left">
						<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_banner_bottom_grid_right">	
						<h4>Certified Products</h4>
						<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<div class="col-md-4 bnr-agileitsgrids w3grid2">
			<div class="agileinfo_banner_bottom_pos">
				<div class="w3_agileits_banner_bottom_pos_grid">
					<div class="col-xs-4 wthree_banner_bottom_grid_left">
						<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
							<i class="fa fa-comment-o" aria-hidden="true"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_banner_bottom_grid_right">	
						<h4>Free Helpline</h4>
						<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div> -->
<!-- //banner-bottom -->
<!-- welcome -->
<div class="welcome">
    <div class="container">
        <div class="col-md-6 w3ls_welcome_right">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="agileits_w3layouts_welcome_grid">
                            <img src="images/about_1_0.png" alt=" " class="img-responsive" />
                        </div>
                    </li>
                    <li>
                        <div class="agileits_w3layouts_welcome_grid">
                            <img src="images/about_2_0.jpg" alt=" " class="img-responsive" />
                        </div>
                    </li>
                    <!-- <li>
                        <div class="agileits_w3layouts_welcome_grid">
                            <img src="images/g3.jpg" alt=" " class="img-responsive" />
                        </div>
                    </li> -->
                </ul>
            </div>
        </div>
        <div class="col-md-6 w3ls_welcome_left">
            <div class="w3ls_welcome_right1">
                <h3 class="agileits-title">About Us</h3>
                <h6>We Provide best quality Products.</h6>
                <p>We Provide you with wide range of products with affordable price.
                    We are leading wholesale of ceramic tiles with variety of product and affordable prices for
                    coustomer and with best quality assurance, coustomer satisfaction. our motto is best quality with
                    affordable price and best services for coustomer.</p>

            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!-- //welcome -->
<!-- services -->
<!-- <div class="services jarallax">
    <div class="container">
        <h3 class="agileits-title w3title1">Our Services</h3>
        <div class="services-w3ls-row">
            <div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <span class="glyphicon fas fa-eye effect-1" aria-hidden="true"></span>
                <h5>OUR VISION</h5>
                <p>To gain international recognization for our products through best quality, coust effectiveness,
                    leadership,innovation.</p>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <span class="glyphicon far fa-chart-bar effect-1" aria-hidden="true"></span>
                <h5>OUR MISSION</h5>
                <p>To become most innovative and valueable an barnd ammount ceramic world.</p>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <span class="glyphicon fas fa-asterisk effect-1" aria-hidden="true"></span>
                <h5>QUAILY ASSURANCE</h5>
                <p>Compnay is commited to provide best quality products being a quality assured firm,we have a special
                    team for quality inspections.</p>
            </div>
           
            <div class="col-md-3 col-sm-3 col-xs-6 services-grid">
					<span class="glyphicon glyphicon-globe effect-1" aria-hidden="true"></span>
					<h5>Phase gravida</h5>
					<p>Itaque earum rerum hic a sapiente delectus in auctor sapien.</p>
				</div> 
            <div class="clearfix"> </div>
        </div>
    </div>
</div> -->

<div class="services jarallax">
    <div class="container">
        <h3 class="agileits-title w3title1">Our Services</h3>
        <div class="services-w3ls-row">
            <div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <span class="glyphicon glyphicon-home effect-1" aria-hidden="true"></span>
                <h5>OUR VISION</h5>
                <p>To Gain International Recognization For Our Products Through Best Quality.</p>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <span class="glyphicon glyphicon-list-alt effect-1" aria-hidden="true"></span>
                <h5>OUR MISSION</h5>
                <p>To Become Most Innovative And Valueable An Brand Ammount Ceramic World.</p>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <span class="glyphicon glyphicon-tree-deciduous effect-1" aria-hidden="true"></span>
                <h5>QUALITY ASSURANCE</h5>
                <p>Company Is Commited To Provide Best Quality Products Being A Quality Assured Firm.</p>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 services-grid">
                <span class="glyphicon glyphicon-globe effect-1" aria-hidden="true"></span>
                <h5>SERVICE</h5>
                <p>We Are Here For Best Services And Client Satisfaction.</p>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>


<div class="welcome">
    <div class="container">

        <div class="col-md-6 w3ls_welcome_left">
            <div class="w3ls_welcome_right1">
                <h3 class="agileits-title">Our Product</h3>
                <h6>We Provide best quality Products.</h6>
                <p>We Provide you with wide range of products with affordable price.
                    We are leading wholesale of ceramic tiles with variety of product and affordable prices for
                    coustomer and with best quality assurance, coustomer satisfaction. our motto is best quality with
                    affordable price and best services for coustomer.</p>

            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="col-md-6 w3ls_welcome_right">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="agileits_w3layouts_welcome_grid">
                            <img src="images/bath tubs 2.jpg" alt=" " class="img-responsive" />
                        </div>
                    </li>
                    <li>
                        <div class="agileits_w3layouts_welcome_grid">
                            <img src="images/toilet_1_0.jpg" alt=" " class="img-responsive" />
                        </div>
                    </li>
                    <li>
                        <div class="agileits_w3layouts_welcome_grid">
                            <img src="images/tiles_1_0.jpg" alt=" " class="img-responsive" />
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>


<!-- //services -->
<!-- blog-bottom -->

<!-- //blog-bottom -->
<!-- news letter -->

<!-- //news letter -->
<!-- footer -->


<?php include('include/footer.php'); ?>


<!-- //footer -->
<!-- //js -->
<!-- jarallax -->
<script type="text/javascript">
/* init Jarallax */
$('.jarallax').jarallax({
    speed: 0.5,
    imgWidth: 1366,
    imgHeight: 768
})
</script>
<!-- //jarallax -->
<!-- ResponsiveTabs js -->
<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#horizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion           
        width: 'auto', //auto or any width like 600px
        fit: true // 100% fit in a container
    });
});
</script>
<!-- //ResponsiveTabs js -->
<!-- banner-type-text -->
<script>
$(function() {

    $("#typed").typed({
        // strings: ["Typed.js is a <strong>jQuery</strong> plugin.", "It <em>types</em> out sentences.", "And then deletes them.", "Try it out!"],
        stringsElement: $('#typed-strings'),
        typeSpeed: 30,
        backDelay: 700,
        loop: true,
        contentType: 'html', // or text
        // defaults to false for infinite loop
        loopCount: false,
        callback: function() {
            foo();
        },
        resetCallback: function() {
            newTyped();
        }
    });

    $(".reset").click(function() {
        $("#typed").typed('reset');
    });

});

function newTyped() {
    /* A new typed object */
}

function foo() {
    console.log("Callback");
}
</script>
<!-- //banner-type-text -->
<!-- flexSlider -->
<script defer src="js/jquery.flexslider.js"></script>
<script type="text/javascript">
$(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider) {
            $('body').removeClass('loading');
        }
    });
});
</script>
<!-- //flexSlider -->
<!-- start-smooth-scrolling -->
<script type="text/javascript">
jQuery(document).ready(function($) {
    $(".scroll").click(function(event) {
        event.preventDefault();

        $('html,body').animate({
            scrollTop: $(this.hash).offset().top
        }, 1000);
    });
});
</script>
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->

<!-- //smooth-scrolling-of-move-up -->
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

</body>

</html>